import React, { Component } from 'react'
import {login} from './chat/actions'
import { connect } from 'react-redux'
import {  Redirect } from 'react-router-dom'

class Login extends Component {

    state = {
        login: '',
        password: ''
    };



    render() {
        let errorMessage;
        if (this.props.error) {
            errorMessage = <div className="notification">{this.props.error}</div>
        } else {
            errorMessage = <div></div>
        }
        const currentUser = this.props.currentUser;
        if ( currentUser === 'admin' ) return <Redirect to="/"/>;
        if ( currentUser ) return <Redirect to="/chat"/>;
        return (
            <div className="login-container">
                {errorMessage}
                <div className="app-name">
                    <h1 className="app-name__h1">Log in Chat</h1>
                </div>
                <form onSubmit={this.handleSubmit} className="hello-box">
                    <div className="hello-box__email">
                        <input value={this.state.login} onChange={this.handleChange('login')} type="text" className="hello-box__input" placeholder="Login"/>
                    </div>
                    <div className="hello-box__password">
                        <input value={this.state.password} onChange={this.handleChange('password')} type="password" className="hello-box__input" placeholder="Password"/>
                    </div>
                    <div className="hello-box__login">
                        <input type="submit" value="Login" className="hello-box__button"/>
                    </div>
                    <div className="hello-box__register">
                        Don’t have an account?
                        <a className="hello-box__link" href="#">Create Account</a>
                    </div>
                </form>
            </div>

        )
    }

    handleChange = (type) => (ev) => {
        this.setState({
            [type]: ev.target.value
        })
    };

    handleSubmit = (ev) => {
        ev.preventDefault();
        this.props.login(this.state);
        this.setState({
            login: '',
            password: ''
        })
    }
}

export default connect((state) => ({
    currentUser: state.chat.currentUser,
    error: state.chat.error
}), {login})(Login);
