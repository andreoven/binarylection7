import axios from 'axios';
import api from '../shared/config/api';
import { call, put, takeEvery, all, select } from 'redux-saga/effects';
import { LOGIN, LOGIN_SUCCESS, FETCH_MESSAGES, FETCH_MESSAGES_SUCCESS,
    SEND_MESSAGE, EDIT_MESSAGE, DELETE_MESSAGE, ERROR } from "./actionTypes";
import {getCurrentTime} from './utils/getCurrentTime';


export function* login(action) {
    const logUser = {login: action.payload.login, password: action.payload.password};
    try {
        const user = yield call(axios.post, `${api.url}/user/login`, logUser);
        yield put({ type: LOGIN_SUCCESS, payload: { userName: user.data } })
    } catch (error) {
        yield put({ type: ERROR, payload: { error: 'Wrong login or password' }})
        console.log('fetchUsers error:', error.message)
    }
}

function* watchLogin() {
    yield takeEvery(LOGIN, login)
}

export function* fetchMessages() {
    try {
        const messages = yield call(axios.get, `${api.url}/message`);
        yield put({ type: FETCH_MESSAGES_SUCCESS, payload: { messages: messages.data } })
    } catch (error) {
        console.log('fetchUsers error:', error.message)
    }
}

function* watchFetchMessages() {
    yield takeEvery(FETCH_MESSAGES, fetchMessages)
}

export function* sendMessage(action) {
    const state = yield select();
    const newMessage = {
        id: (parseInt(state.chat.messages[state.chat.messages.length - 1].id) + 10).toString(),
        user: state.chat.currentUser,
        created_at: getCurrentTime(),
        message: action.payload
    };
    try {
        yield call(axios.post, `${api.url}/message`, newMessage);
        yield put({ type: FETCH_MESSAGES })
    } catch (error) {
        console.log('createUser error:', error.message);
    }
}

function* watchSendMessage() {
    yield takeEvery(SEND_MESSAGE, sendMessage)
}

export function* editMessage(action) {
    const newMessage = { message: action.payload.message};
    try {
        yield call(axios.put, `${api.url}/message/${action.payload.id}`, newMessage);
        yield put({ type: FETCH_MESSAGES })
    } catch (error) {
        console.log('createUser error:', error.message);
    }
}

function* watchEditMessage() {
    yield takeEvery(EDIT_MESSAGE, editMessage)
}

export function* deleteMessage(action) {
    try {
        yield call(axios.delete, `${api.url}/message/${action.payload}`);
        yield put({ type: FETCH_MESSAGES })
    } catch (error) {
        console.log('createUser error:', error.message);
    }
}

function* watchDeleteMessage() {
    yield takeEvery(DELETE_MESSAGE, deleteMessage)
}

export default function* chatSagas() {
    yield all([
        watchLogin(),
        watchFetchMessages(),
        watchSendMessage(),
        watchEditMessage(),
        watchDeleteMessage()
    ])
};
