import { LOGIN_SUCCESS, FETCH_MESSAGES_SUCCESS, ERROR } from "./actionTypes";

const initialState = {
    messages: [],
    currentUser: '',
    error: '',
};

export default function (state = initialState, action) {
    switch (action.type) {
        case LOGIN_SUCCESS: {
            const  user  = action.payload.userName;
            return {
                ...state,
                currentUser: user
            };
        }

        case FETCH_MESSAGES_SUCCESS: {
            return {
                ...state,
                messages: action.payload.messages
            }
        }

        case ERROR: {
            return {
                ...state,
                error: action.payload.error
            }
        }

        default:
            return state;
    }
}