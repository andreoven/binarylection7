import { LOGIN, FETCH_MESSAGES, SEND_MESSAGE, EDIT_MESSAGE, DELETE_MESSAGE } from "./actionTypes";

export const login = ({login, password}) => ({
   type: LOGIN,
   payload: {
       login,
       password
   }
});

export const fetchMessages = () => ({
   type:  FETCH_MESSAGES
});

export const sendMessage = (data) => ({
    type:  SEND_MESSAGE,
    payload: data
});

export const editMessage = (id, message) => ({
    type:  EDIT_MESSAGE,
    payload: {id, message}
});

export const deleteMessage = (data) => ({
    type:  DELETE_MESSAGE,
    payload: data
});