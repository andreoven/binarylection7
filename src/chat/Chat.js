import React from 'react';
import Header from "./Header";
import MessageList from "./MessageList";
import MessageInput from "./MessageInput";
import {connect} from 'react-redux'
import { fetchMessages } from './actions';
import Spinner from './Spinner';
import {Redirect} from "react-router";

class Chat extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isLoaded: false
        }
    }

    componentDidMount() {
        const {fetchMessages} = this.props;
        fetchMessages();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.showSpinner();
    }

    getHeaderInfo(users) {
        let usersCount = new Set();
        users.forEach(user => {
            usersCount.add(user.user);
        });
        let messageCount = users.length;
        return {
            users: usersCount.size,
            messages: messageCount,
            lastMessage: users[users.length - 1].created_at
        }
    }

    showSpinner() {
        if (!this.state.isLoaded) {
            if (this.props.messages.length > 0) {
                this.setState({isLoaded: true})
            }
        }
    }

    render() {
        const currentUser = this.props.currentUser;
        if ( !currentUser ) return <Redirect to="/login"/>;
        if (this.state.isLoaded) {
            let headerInfo = this.getHeaderInfo(this.props.messages);
            return (
                <div className="Chat">
                    <Header headerInfo = {headerInfo} />
                    <MessageList users = {this.props.messages} currentUser = {this.props.currentUser}  />
                    <MessageInput  />
                </div>
            );
        } else {
            return <Spinner/>
        }


    }
}

export default connect(
    (state) => ({
        messages: state.chat.messages,
        currentUser: state.chat.currentUser
    }),{fetchMessages})(Chat);