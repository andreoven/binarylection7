import React from 'react';
import {connect} from 'react-redux'
import { sendMessage } from './actions'

class MessageInput extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            text: ''
        };
    }

    onChange(e) {
        this.setState({
            text: e.target.value
        })
    }

    clearInput(text) {
        const sendMessage = this.props.sendMessage;
        sendMessage(text);
        this.setState({
            text: ''
        });
    }

    render() {
        return (
            <div className="MessageInput">
                <label>New message: </label>
                <input type="text" value={ this.state.text } onChange={ (e) => this.onChange(e) }/>
                <p onClick={() => this.clearInput(this.state.text)}>Send</p>
            </div>
        );
    }
}

export default connect(null, { sendMessage })(MessageInput);