import React from 'react';


function Line(props) {
        const separator = props.separator;
        if (separator) {
            return (
                <div className="Separator">
                    <div className="left-line"><div className="line"></div></div>
                    <div className="separator-date">
                        <p>{separator}</p>
                    </div>
                    <div className="right-line"><div className="line"></div></div>
                </div>
            );
        } else {
            return (
                <div className="Separator">
                </div>
            );
        }

}

export default Line;