import React from 'react';


function Header(props) {
        let date = (new Date(props.headerInfo.lastMessage)).toString();
        let dateArray = date.split(" ");
        return (
            <div className="Header">
                <p>My Chat</p>
                <p>{props.headerInfo.users} Participants</p>
                <p>{props.headerInfo.messages} Messages</p>
                <p>Last message at: {dateArray[0]}, {dateArray[1]} {dateArray[2]}</p>
            </div>
        );
}

export default Header;