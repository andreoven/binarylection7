export const getCurrentTime = () => {
    let yyyy = new Date().getFullYear();
    let mm = new Date().getMonth()+1;
    let dd = new Date().getDate();

    let hours = new Date().getHours();
    let minutes = new Date().getMinutes();
    let seconds = new Date().getSeconds();

    return yyyy + "-" + mm + "-" + dd +  " " + hours + ":" + minutes + ":" + seconds;
};