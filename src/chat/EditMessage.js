import React from 'react';

class EditMessage extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            text: ''
        }
    }

    componentDidMount() {
        this.setState({
            text: this.props.text
        })
    }

    onChange = (e) => {
        this.setState({
            text: e.target.value
        })
    }

    render() {
        return (
            <div className="EditMessage">
                <p>Edit message</p>
                <textarea type="text" value={ this.state.text } onChange={ (e) => this.onChange(e) }/>
                <button onClick={() => this.props.closeInput(this.props.user.id, this.state.text)}>Save</button>
            </div>
        );
    }
}

export default EditMessage;