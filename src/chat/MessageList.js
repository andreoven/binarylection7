import React from 'react';
import Message from './Message';


function MessageList(props) {
        const users = props.users;
        let date = (users[0].created_at).toString().split(" ")[0];
        const messageItems = users.map((user, index) => {
            if (date === (user.created_at).toString().split(" ")[0]) {
                return (
                    <li key={user.id}>
                        <Message user = {user} isSeparator = {false} currentUser = {props.currentUser}  />
                    </li>
                )
            } else {
                let dateSeparator = date;
                date = (user.created_at).toString().split(" ")[0];
                return (
                    <li key={user.id}>
                        <Message user = {user} isSeparator={dateSeparator} currentUser = {props.currentUser}  />
                    </li>
                )
            }
        });

        return (
            <div className="MessageList">
                {messageItems}
            </div>
        );

}

export default MessageList;