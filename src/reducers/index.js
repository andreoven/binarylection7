import { combineReducers } from "redux";
import users from "../users/reducer";
import userPage from "../userPage/reducer";
import chat from "../chat/reducer";

const rootReducer = combineReducers({
    users,
    userPage,
    chat
});

export default rootReducer;
