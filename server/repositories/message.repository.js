const fs = require('fs');


const  getAllMessages =  () => {
    let data = JSON.parse(fs.readFileSync('messages.json'));
    return data;
};

const rewriteMessages = (newData) => {
    fs.writeFile('messages.json', '', (err) => {
        if (err) console.log(err);
        fs.writeFile('messages.json', newData, (err) => {
            if (err) console.log(err);
        });
    });
};

module.exports = {
    getAllMessages,
    rewriteMessages
};
