const fs = require('fs');


const  getAllUsers =  () => {
    //get data from userlist.json
    let data = JSON.parse(fs.readFileSync('userlist.json'));
    return data;
};

const rewriteUsers = (newData) => {
    //Clearing file by writing empty string and writing a new data
    fs.writeFile('userlist.json', '', (err) => {
        if (err) console.log(err);
        fs.writeFile('userlist.json', newData, (err) => {
            if (err) console.log(err);
        });
    });
};


module.exports = {
    getAllUsers,
    rewriteUsers
};