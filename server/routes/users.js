var express = require('express');
var router = express.Router();


const { getUsers } = require('../services/user.service');
const { getUserById } = require('../services/user.service');
const { createUser } = require('../services/user.service');
const { updateUser } = require('../services/user.service');
const { deleteUser } = require('../services/user.service');
const { loginUser } = require('../services/user.service');

/* GET users listing. */
router.get('/', function(req, res, next) {
    let result = getUsers();
    if (result) {
        res.send(result);
    } else {
        res.status(400).send('Some error');
    }
});

router.get('/:id', function(req, res, next) {
    let result = getUserById(req.params.id);
    if (result){
        res.send(result);
    }  else {
        res.status(400).send('Incorrect id');
    }
});

router.post('/', function(req, res, next) {
    let result = createUser(req.body);
    if (result) {
        res.send('ok');
    } else {
        res.status(400).send('Incorrect format of user');
    }
});

router.post('/login', function(req, res, next) {
    let result = loginUser(req.body);
    if (result) {
        res.send(result);
    } else {
        res.status(400).send('Invalid data');
    }
});

router.put('/:id', function(req, res, next) {
    let result = updateUser(req.params.id, req.body);
    if (result){
        res.send(result);
    }  else {
        res.status(400).send('Some error');
    }
});

router.delete('/:id', function(req, res, next) {
    let result = deleteUser(req.params.id);
    if (result){
        res.send(result);
    } else {
        res.status(400).send('Some error');
    }
});


module.exports = router;
