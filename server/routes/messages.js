var express = require('express');
var router = express.Router();
const { getMessages } = require('../services/message.service');
const { addMessage } = require('../services/message.service');
const { updateMessage } = require('../services/message.service');
const { deleteMessage } = require('../services/message.service');


router.get('/', function(req, res, next) {
    let result = getMessages();
    if (result) {
        res.send(result);
    } else {
        res.status(400).send('Some error');
    }
});

router.post('/', function(req, res, next) {
    let result = addMessage(req.body);
    if (result) {
        res.send('ok');
    } else {
        res.status(400).send('Incorrect format of user');
    }
});

router.put('/:id', function(req, res, next) {
    let result = updateMessage(req.params.id, req.body);
    if (result){
        res.send('ok');
    }  else {
        res.status(400).send('Some error');
    }
});

router.delete('/:id', function(req, res, next) {
    let result = deleteMessage(req.params.id);
    if (result){
        res.send('ok');
    } else {
        res.status(400).send('Some error');
    }
});


module.exports = router;
