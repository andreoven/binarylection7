const validate = require("validate.js");
var _ = require('lodash');

const { getAllUsers } = require("../repositories/user.repository");
const { rewriteUsers } = require("../repositories/user.repository");
const { constraints } = require("../utils/validator");
const { idConstraints } = require("../utils/validator");


const getUsers =  () => {
    return getAllUsers();
};

const getUserById = (id) => {

        let data = getAllUsers();
        const user = data.filter(user => user.id === id);
        if (user.length <= 0) {
            return false;
        }
        return user[0];

};

const loginUser = (body) => {
        let newUser;
        let data = getAllUsers();
        data.forEach((user) => {
            if (user.name === body.login && user.password === body.password) {
                newUser = user;
            }
        });
        return newUser.name;

};

const createUser = (userData) => {
    const allUsers = getAllUsers();
    let newUser;
    if (_.isArray(userData)) {
        newUser = userData[0];
    } else {
        newUser = userData;
    }
        newUser._id = allUsers.length + 1;
        allUsers.push(newUser);
        rewriteUsers(JSON.stringify(allUsers));
        return true;

};

const updateUser = (id, userData) => {

    const allUsers = getAllUsers();
    let newUser;
    if (_.isArray(userData)) {
        newUser = userData[0];
    } else {
        newUser = userData;
    }
    console.log(newUser);
        let userIndex = _.findIndex(allUsers, element => element.id == id);
        allUsers[userIndex].name = newUser.name;
        allUsers[userIndex].surname = newUser.surname;
        allUsers[userIndex].email = newUser.email;
        allUsers[userIndex].password = newUser.password;
        rewriteUsers(JSON.stringify(allUsers));
        return true;

};

const deleteUser = (id) => {
    const allUsers = getAllUsers();
    let userIndex = _.findIndex(allUsers, element => element.id == id);
    allUsers.splice(userIndex, 1);
    rewriteUsers(JSON.stringify(allUsers));
    return true;
};


module.exports = {
    getUsers,
    getUserById,
    createUser,
    updateUser,
    deleteUser,
    loginUser
};
