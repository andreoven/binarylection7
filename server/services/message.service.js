var _ = require('lodash');
const { getAllMessages } = require("../repositories/message.repository");
const { rewriteMessages } = require("../repositories/message.repository");


const getMessages =  () => {
    return getAllMessages();
};

const addMessage = (messageData) => {
    const allMessages = getAllMessages();
    allMessages.push(messageData);
    rewriteMessages(JSON.stringify(allMessages));
    return true;
};

const updateMessage = (id, userData) => {
    const allMessages = getAllMessages();
    let messageIndex = _.findIndex(allMessages, element => element.id == id);
    allMessages[messageIndex].message = userData.message;
    rewriteMessages(JSON.stringify(allMessages));
    return true;
};

const deleteMessage = (id) => {
    const allMessages = getAllMessages();
    let messageIndex = _.findIndex(allMessages, element => element.id == id);
    allMessages.splice(messageIndex, 1);
    rewriteMessages(JSON.stringify(allMessages));
    return true;
};

module.exports = {
    getMessages,
    addMessage,
    updateMessage,
    deleteMessage
};
