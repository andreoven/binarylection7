var express = require('express');
var app = express();
const server = require('http').Server(app);
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var messagesRouter = require('./routes/messages');

server.listen(3000);


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());


app.use('/', indexRouter);
app.use('/user', usersRouter);
app.use('/message', messagesRouter);

module.exports = app;
